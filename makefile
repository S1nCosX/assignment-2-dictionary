ASM=nasm
ASM_FLAGS=-g -felf64
LD=ld
RM=rm

build: main clean

main: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: src/%.asm
	$(ASM) $(ASM_FLAGS) -o $@ $^

clean:
	$(RM) *.o

.PHONY: build
