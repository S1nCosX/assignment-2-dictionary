;lib.inc

extern exit

extern print_char
extern print_int
extern print_newline
extern print_string
extern print_uint
extern print_err

extern parse_int
extern parse_uint

extern string_length
extern string_equals

extern read_char
extern read_word
extern read_string
