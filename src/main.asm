;main.asm

%include "src/lib.inc"
%include "src/colon.inc"
%include "src/words.inc"

extern find_word

section .rodata
    BUFFER_SIZE: db 256
    ID_SIZE: db 8

section .data
    inp_err: db "Input error", 0
    no_key_err: db "There no key like this", 0

section .bss 
    buffer: resb BUFFER_SIZE

section .text

global _start

read:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_string ;считываем строку

    ;проверяем, что введенное корректно
    push rax ;сохраняем rax от затирания
    test rax, rax ;если rax = 0, то произошла ошибка и строка введена не корректно
        jz .err
    pop rax
ret
.err:
    mov rdi, inp_err
    call print_err
    call exit


_start:
    call read
    mov rdi, rax
    mov rsi, NEXT
    call find_word
    
    ;проверяем, нашли ли мы слово в словаре
    push rax ;сохраняем rax от затирания
    test rax, rax ;если rax = 0, то произошла ошибка и строка введена не корректно
        jz .err
    pop rax

    mov rdi, rax ;раз у нас не вывело ошибку, то слово есть
    add rdi, ID_SIZE ;пропускаем указатель
    call string_length ;находим длинну ключа
    inc rdi
    add rdi, rax
    call print_string ;выводим ответ
    call print_newline
call exit
.err:
    mov rdi, no_key_err
    call print_err
    call exit
