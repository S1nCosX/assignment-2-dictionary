;dict.asm

%include "src/lib.inc" 
%define ID_SIZE 8 ;размер указателя

section .text

global find_word

find_word:
    ;(rdi = слово, rsi = начало словаря, )
    .loop:
    push rsi ;сохраняем данные
    push rdi ;от затирания

    add rsi, ID_SIZE

    call string_equals ;ответ будет в rax

    pop rdi ;возвращаем
    pop rsi ;данные

    cmp rax, 1 ;проверка на то, нашли ли нужное
        je .goodOut

    mov rsi, [rsi]

    test rsi, rsi
    jnz .loop

.badOut: ;не нашли, возвращаем 0
    xor rax, rax
    ret

.goodOut: ;нашли, возвращаем указатель
    mov rax, rsi
 ret