%define NEXT 0

%macro colon 2
    %ifid %2
        %ifstr %1 
            %2: dq NEXT
            db %1 , 0 
            %define NEXT %2
        %else
            %error "Fist variable must be String"
        %endif
    %else
        %error "Second variable must be Id"
    %endif
%endmacro